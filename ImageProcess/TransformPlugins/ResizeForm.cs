﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace ImageProcessPlugins
{
    public partial class ResizeForm : Form
    {
        public ResizeForm()
        {
            InitializeComponent();
        }

        public int OriWidth=1, OriHeight=1;

        private void ResizeForm_Load(object sender, EventArgs e)
        {
            widthTextBox.Text = OriWidth.ToString();
            heightTextBox.Text = OriHeight.ToString();
        }

        private void widthTextBox_TextChanged(object sender, EventArgs e)
        {
            if (chkLockRatio.Checked)
            {
                try
                {
                    heightTextBox.TextChanged -= new EventHandler(heightTextBox_TextChanged);
                    heightTextBox.Text = Math.Round(OriHeight * int.Parse(widthTextBox.Text) / (double)OriWidth).ToString();
                    heightTextBox.TextChanged += new EventHandler(heightTextBox_TextChanged);
                }
                catch (Exception)
                { }
            }
        }

        private void heightTextBox_TextChanged(object sender, EventArgs e)
        {
            if (chkLockRatio.Checked)
            {
                try
                {
                    widthTextBox.TextChanged -= new EventHandler(widthTextBox_TextChanged);
                    widthTextBox.Text = Math.Round(OriWidth * int.Parse(heightTextBox.Text) / (double)OriHeight).ToString();
                    widthTextBox.TextChanged += new EventHandler(widthTextBox_TextChanged);
                }
                catch (Exception)
                { }
            }
        }

        public int NewWidth=1, NewHeight=1;

        private void okButton_Click(object sender, EventArgs e)
        {
            try
            {
                NewWidth = int.Parse(widthTextBox.Text);
                NewHeight = int.Parse(heightTextBox.Text);
                if (NewWidth <= 0 || NewWidth >= 65536)
                    throw new Exception();
                if (NewHeight <= 0 || NewHeight >= 65536)
                    throw new Exception();
                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a valid size.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
