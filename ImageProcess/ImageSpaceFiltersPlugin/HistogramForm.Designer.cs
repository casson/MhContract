﻿namespace ImageSpaceFiltersPlugin
{
    partial class HistogramForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.histogramEqualizationButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.histogramPanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.histogramEqualizationButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(341, 30);
            this.panel1.TabIndex = 0;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(231, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // histogramEqualizationButton
            // 
            this.histogramEqualizationButton.Location = new System.Drawing.Point(5, 4);
            this.histogramEqualizationButton.Name = "histogramEqualizationButton";
            this.histogramEqualizationButton.Size = new System.Drawing.Size(151, 23);
            this.histogramEqualizationButton.TabIndex = 1;
            this.histogramEqualizationButton.Text = "Histogram &Equalization";
            this.histogramEqualizationButton.UseVisualStyleBackColor = true;
            this.histogramEqualizationButton.Click += new System.EventHandler(this.histogramEqualizationButton_Click);
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(162, 4);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(63, 23);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // histogramPanel
            // 
            this.histogramPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.histogramPanel.Location = new System.Drawing.Point(5, 5);
            this.histogramPanel.Name = "histogramPanel";
            this.histogramPanel.Padding = new System.Windows.Forms.Padding(5);
            this.histogramPanel.Size = new System.Drawing.Size(331, 392);
            this.histogramPanel.TabIndex = 1;
            this.histogramPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.histogramPanel_Paint);
            this.histogramPanel.Resize += new System.EventHandler(this.histogramPanel_Resize);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.histogramPanel);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 30);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(5);
            this.panel2.Size = new System.Drawing.Size(341, 402);
            this.panel2.TabIndex = 2;
            // 
            // HistogramForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(341, 432);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimizeBox = false;
            this.Name = "HistogramForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Histogram";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel histogramPanel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button histogramEqualizationButton;
        private System.Windows.Forms.Button cancelButton;
    }
}