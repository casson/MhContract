﻿namespace Sbk_Manage
{
    partial class Frm_EditHt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_EditHt));
            this.label1 = new System.Windows.Forms.Label();
            this.txtHtbh = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtXmbh = new System.Windows.Forms.TextBox();
            this.txtHtmc = new System.Windows.Forms.TextBox();
            this.txtZbdlgs = new System.Windows.Forms.TextBox();
            this.TxtHte = new System.Windows.Forms.TextBox();
            this.txtGhs = new System.Windows.Forms.TextBox();
            this.txtGhsYhk = new System.Windows.Forms.TextBox();
            this.txtLrr = new System.Windows.Forms.TextBox();
            this.dtpHtQdrq = new System.Windows.Forms.DateTimePicker();
            this.dtpHtYsrq = new System.Windows.Forms.DateTimePicker();
            this.chKYys = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "合同编号：";
            // 
            // txtHtbh
            // 
            this.txtHtbh.Location = new System.Drawing.Point(130, 11);
            this.txtHtbh.MaxLength = 100;
            this.txtHtbh.Name = "txtHtbh";
            this.txtHtbh.ReadOnly = true;
            this.txtHtbh.Size = new System.Drawing.Size(245, 26);
            this.txtHtbh.TabIndex = 1;
            this.txtHtbh.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "项目编号：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "合同名称：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "招标代理公司：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "合同额(万¥)：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 169);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "供货商：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(-2, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(136, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "供货商银行账户：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(62, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 16);
            this.label8.TabIndex = 1;
            this.label8.Text = "录入人：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 262);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "合同签订日期：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 293);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "合同验收日期：";
            // 
            // txtXmbh
            // 
            this.txtXmbh.Location = new System.Drawing.Point(130, 42);
            this.txtXmbh.MaxLength = 100;
            this.txtXmbh.Name = "txtXmbh";
            this.txtXmbh.Size = new System.Drawing.Size(245, 26);
            this.txtXmbh.TabIndex = 2;
            // 
            // txtHtmc
            // 
            this.txtHtmc.Location = new System.Drawing.Point(130, 73);
            this.txtHtmc.MaxLength = 100;
            this.txtHtmc.Name = "txtHtmc";
            this.txtHtmc.Size = new System.Drawing.Size(245, 26);
            this.txtHtmc.TabIndex = 3;
            // 
            // txtZbdlgs
            // 
            this.txtZbdlgs.Location = new System.Drawing.Point(130, 104);
            this.txtZbdlgs.MaxLength = 100;
            this.txtZbdlgs.Name = "txtZbdlgs";
            this.txtZbdlgs.Size = new System.Drawing.Size(245, 26);
            this.txtZbdlgs.TabIndex = 4;
            // 
            // TxtHte
            // 
            this.TxtHte.Location = new System.Drawing.Point(130, 135);
            this.TxtHte.MaxLength = 100;
            this.TxtHte.Name = "TxtHte";
            this.TxtHte.Size = new System.Drawing.Size(245, 26);
            this.TxtHte.TabIndex = 5;
            // 
            // txtGhs
            // 
            this.txtGhs.Location = new System.Drawing.Point(130, 166);
            this.txtGhs.MaxLength = 100;
            this.txtGhs.Name = "txtGhs";
            this.txtGhs.Size = new System.Drawing.Size(245, 26);
            this.txtGhs.TabIndex = 6;
            // 
            // txtGhsYhk
            // 
            this.txtGhsYhk.Location = new System.Drawing.Point(130, 197);
            this.txtGhsYhk.MaxLength = 100;
            this.txtGhsYhk.Name = "txtGhsYhk";
            this.txtGhsYhk.Size = new System.Drawing.Size(245, 26);
            this.txtGhsYhk.TabIndex = 7;
            // 
            // txtLrr
            // 
            this.txtLrr.Location = new System.Drawing.Point(130, 228);
            this.txtLrr.MaxLength = 100;
            this.txtLrr.Name = "txtLrr";
            this.txtLrr.ReadOnly = true;
            this.txtLrr.Size = new System.Drawing.Size(245, 26);
            this.txtLrr.TabIndex = 8;
            // 
            // dtpHtQdrq
            // 
            this.dtpHtQdrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpHtQdrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHtQdrq.Location = new System.Drawing.Point(130, 260);
            this.dtpHtQdrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHtQdrq.Name = "dtpHtQdrq";
            this.dtpHtQdrq.Size = new System.Drawing.Size(141, 26);
            this.dtpHtQdrq.TabIndex = 9;
            // 
            // dtpHtYsrq
            // 
            this.dtpHtYsrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpHtYsrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHtYsrq.Location = new System.Drawing.Point(130, 291);
            this.dtpHtYsrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpHtYsrq.Name = "dtpHtYsrq";
            this.dtpHtYsrq.Size = new System.Drawing.Size(141, 26);
            this.dtpHtYsrq.TabIndex = 10;
            // 
            // chKYys
            // 
            this.chKYys.AutoSize = true;
            this.chKYys.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.chKYys.Location = new System.Drawing.Point(275, 294);
            this.chKYys.Name = "chKYys";
            this.chKYys.Size = new System.Drawing.Size(75, 20);
            this.chKYys.TabIndex = 12;
            this.chKYys.Text = "已验收";
            this.chKYys.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Image = global::Sbk_Manage.Properties.Resources.ok_16px_1134004_easyicon_net;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(143, 319);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 36);
            this.button1.TabIndex = 11;
            this.button1.Text = "确认更新";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Frm_EditHt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 356);
            this.Controls.Add(this.chKYys);
            this.Controls.Add(this.dtpHtYsrq);
            this.Controls.Add(this.dtpHtQdrq);
            this.Controls.Add(this.txtLrr);
            this.Controls.Add(this.txtGhsYhk);
            this.Controls.Add(this.txtGhs);
            this.Controls.Add(this.TxtHte);
            this.Controls.Add(this.txtZbdlgs);
            this.Controls.Add(this.txtHtmc);
            this.Controls.Add(this.txtXmbh);
            this.Controls.Add(this.txtHtbh);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Frm_EditHt";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "编辑合同信息";
            this.Load += new System.EventHandler(this.Frm_AddHt_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtHtbh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtXmbh;
        private System.Windows.Forms.TextBox txtHtmc;
        private System.Windows.Forms.TextBox txtZbdlgs;
        private System.Windows.Forms.TextBox TxtHte;
        private System.Windows.Forms.TextBox txtGhs;
        private System.Windows.Forms.TextBox txtGhsYhk;
        private System.Windows.Forms.TextBox txtLrr;
        internal System.Windows.Forms.DateTimePicker dtpHtQdrq;
        internal System.Windows.Forms.DateTimePicker dtpHtYsrq;
        private System.Windows.Forms.CheckBox chKYys;
    }
}